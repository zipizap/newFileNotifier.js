/*
	mFile() 
		- is a constructor function to create objects, which each represent a file path - it's a simple wrapper to file paths, with some convenient methods
	  - it's used to represent files
  
	mFiler()
		- is a contructor function -the only instance used in the program is the object "mfiler"
		- it is just a simple queue, that has a method .addIfNewFile() to add-and-check if the argument passed is already registered in mFiler.
	
	scanDirForExtFiles()
		- it's a function, not a contructor function, nor anything more complex - a function just like in C-style
		- it's like a simple "ls -l *.zip" made with function handler
		
	executeCommandIfNewFile()
		- it's a function, not a contructor function, nor anything more complex - a function just like in C-style

*/

var sys = require("sys");
var posix = require("posix");
var Path=require("path");

//Functions and objects definitions
var userDefined = {
	target : {
		dirPath: "./testDir", //will be a overriden by command line argument supplied
		fileExt: ".zip"  //will be a overriden by  a command line argument supplied
	},
	logDir : "", 				//will be a overriden by  a command line argument supplied
	command: "ls -l" //will be a overriden by  a command line argument supplied
}

{// argument check and info printout
  {// Define version
		var version="1.0"; //version of the program to display in usage help
  }
  {// Define usageStr
    var usageStr=""+
      "   newFileNotifier.js v"+ version +" [https://github.com/zipizap/newFileNotifier.js]
      "\n"+
      "   Usage: \n"+
      "      node newFileNotifier.js <directory> <file-extension> <command> <logsDir>\n" +
      "\n"+
      "   This program, will continuously monitor the <directory> for any new files created with extension <file-extension>\n" +
      "   For each new file detected, it will create one child process executing: \n"+
      '      "<command> NEW_FILE", where NEW_FILE will be substituted by the absolute path of the new file detected'+"\n"+ 
      "   After executing the command, the stoud and stderr of the execution will be logged inside the directory <logsDir>\n"+
			"   This program, existing through node.js, has native asynchronous calls, meaning the monitoring and child-processes\n"+
      "   execution is non-blocking, memory-efficient and parallel\n" +
      "   Note that this program will only monitor for new files creation, which does not include if an existing file is renamed to\n"+
      "   a new name (it continues to be an existing file, only with a different file name)\n"+
      "\n"+
      "   Arguments:\n"+
      "     <directory>\n"+
      "          The directory to be monitored for new files. It can be a full or relative path to the directory\n"+
      "     <file-extension>\n"+
      "          All the new files created within <directory> will be detected, but only the new files with the same extension as \n"+
      "          <file-extension> will be processed (all others will be ignored).\n"+
      "          If the extension is defined as \".*\" then any extension will be detected.\n"+
      "     <command>\n"+
      "          The command to be run in the child-process. This program will be called with one argument - the new file's full path\n"+
			"     <logsDir>\n"+
			"          The directory were it will be saved the logs of the command execution. The logs will be one file for stdout and\n"+
			"           another file for stderr (if it exists)\n" + 
      "   Example:\n"+
      '     node newFileNotifier.js ./testDir zip "ls -l" "./testLogs"'+"\n" +
      "      (now create a new file \"touch $(date +%Y%m%d-%H%M%S).zip\" inside testDir and the see the output of the program)\n"+
      "\n\n";
  }
  { // check arguments and show info (usageStr)
    if ( ( process.argv.length < 6 ) ) {
      //Too few arguments
      sys.puts(usageStr);
      sys.puts("ERROR: Too few arguments!!!");
      var helperStr= "" + 
        ( (process.argv.length >= 3) ? "  <directory> = "+process.argv[2]+"\n" : "") + 
        ( (process.argv.length >= 4) ? "  <file-extension> = "+process.argv[3]+"\n" : "") + 
        ( (process.argv.length >= 5) ? "  <command> = "+process.argv[4] : "");
        ( (process.argv.length >= 6) ? "  <logDir> = "+process.argv[5] : "");				
      sys.puts(helperStr+"\n");
      return 1;
    }
    else if (process.argv.length > 6) {
      //Too much arguments
      sys.puts(usageStr);
      sys.puts("ERROR: Too much arguments!!!");
      var helperStr= "" + 
        ( (process.argv.length >= 3) ? "  <directory> = "+process.argv[2]+"\n" : "") + 
        ( (process.argv.length >= 4) ? "  <file-extension> = "+process.argv[3]+"\n" : "") + 
        ( (process.argv.length >= 5) ? "  <command> = "+process.argv[4]+"\n" : "") + 
        ( (process.argv.length >= 6) ? "  <logDir> = "+process.argv[5]+"\n" : "") + 				
        ( (process.argv.length >= 7) ? "  !!argument in excess #1!! = "+process.argv[6]+"\n" : "") +
        ( (process.argv.length >= 8) ? "  !!argument in excess #2!! = "+process.argv[7]+"\n" : "") +
        ( (process.argv.length >= 9) ? "     ...   \n" : "") + 
        "  (Guess: If you are trying to use spaces or * inside an argument, be sure to surround the argument with double quotes \"this is considered only *one* argument\" )"
      sys.puts(helperStr+"\n");
      return 1;
    }
  }
  {//Assign arguments into userDefined.xxx
    userDefined.target.dirPath=process.argv[2];
		userDefined.target.dirPath=userDefined.target.dirPath.replace(/\/$/,""); // remove trailing \ if exists
    userDefined.target.fileExt=process.argv[3];
		userDefined.target.fileExt=userDefined.target.fileExt.replace(/^[^.]*$/,".$&"); // insert startin . if doesn't exist
    userDefined.command=process.argv[4];
    userDefined.logDir=process.argv[5];
		userDefined.logDir=userDefined.logDir.replace(/\/$/,""); // remove trailing \ if exists
  }
}

function mFile(filenameWithPathStr) {
	this.fullFileName = filenameWithPathStr;  					//  /home/fagulhas/progs/newFileNotifier.js/targetDir/asd.zip
	this.fileName = Path.basename(this.fullFileName); 	//  asd.zip 
	this.fileExt = Path.extname(this.fileName);		//  .zip
	this.dirName = Path.dirname(this.fullFileName);		//  /home/fagulhas/progs/newFileNotifier.js/targetDir
	//this.ctime=posix.stat(this.fullFileName).wait().ctime; // commente ctime changes (line 116 and 143)
	return this;
}
function mFiler() {
	var queue = []; // Queue of known files
	var self=this;
	this.addIfNewFile=function add(mfile) {
		/*
		  - check if mfile is new: isItNew(mfile)
				- If no:
					return false
				- If yes:
					Add mfile to queue of known files: enqueue(mfile)
					return true
		*/
		if (isItNew(mfile) == false) {
			//sys.puts("> Not a new file");
      return false;
    }
		else {
			//sys.puts("> It's a new file!!");
      enqueue(mfile);
			return true;
		}
	} //end of this.addIfNewFile
	function isItNew(mfile) {
	for (var i=0; i<queue.length; i++) {
			if ( (String(queue[i].fullFileName) == String(mfile.fullFileName)) )  { //commented ctime changes: && (String(queue[i].ctime) == String(mfile.ctime))  ) {
				/*NOTE about the if conditions and String()
							I've wrapped inside String() each of the operands of the conditions, so that the comparisons are made on a s tring basis, and not on a object basis (as it happened with .ctime)
				*/
				return false;
      }
    }
		return true;
	}
	function enqueue(mfile) {
    //sys.puts(">> Enqueued: "+mfile.fullFileName);
    queue.push(mfile);
		return;
	}
}
function scanDirForExtFiles(theDir, theExt, funcHandler /* [, funcHandler-arg1, funcHandler-arg2, ...] */) {
   /* 
				For each file inside theDir, with extension theExt it will:
					call funcHandler(mfile [[[, funcHandler-arg1], funcHandler-arg2], ...]) passing as argument an mFile object corresponding to the new file and any other funcHandler-args supplied
        If theExt=".*" then all files of any extension will be parsed
   */
  var scanDir_arguments=arguments;
	posix.readdir(theDir).addCallback(
		function (dirList) {
			for (var i=0; i<dirList.length; i++) {
				var mfile_i=new mFile(theDir + "/" + dirList[i]);
				//sys.puts("-> "+mfile_i.fullFileName);
				if ( (theExt == ".*") || ( theExt == mfile_i.fileExt) ) {
					//mfile_i extension matches
					//sys.puts("ext match");
					var funcHandlerCallStr="funcHandler(mfile_i";
					for (var j=3; j<scanDir_arguments.length; j++) {
						funcHandlerCallStr+=", scanDir_arguments["+j+"]";
					}
          funcHandlerCallStr+=")";
          //sys.puts("-- Going to eval '"+funcHandlerCallStr+"'");
          eval( funcHandlerCallStr);
				}
			}
		}
	);
}
function executeCommandIfNewFile(mfile_i, theCommand, logDir) {
	/*
    This function will execute in a child-process "theCommand mfile_i.fullFileName" if the mfile_i is a new file (a file not yet enqueued in mfiler) 
    After executing, it will display if the execution terminated with success/error and send to stoud and stderr into log files saved inside logDir.
  */
  function saveLogsStdoutStderr(stdout, stderr, logDir, mfile_i) { // a inner function, to avoid code repetition
		/*
			It will:
				- define:
					var DateNow = YYYYMMDDHHMMSS
					var logFileStdout = logDir/mfile_i.fileName.DateNow.stdout
					var logFileStderr = logDir/mfile_i.fileName.DateNow.stderr
				- save logFileStdout and logFileStderr
				- return
		*/
		{// define DateNow, logFileStdout, logFileStderr
			var now=new Date();
			var DateNow = "" +	 // YYYYMMDDHHMMSS
					now.getFullYear() + //YYYY
					(String(Number(now.getMonth() + 1))).replace(/^\d$/,"0$&") + // MM
					(String(now.getDate())).replace(/^\d$/,"0$&") +  //DD
					(String(now.getHours())).replace(/^\d$/,"0$&") + //HH
					(String(now.getMinutes())).replace(/^\d$/,"0$&") + //MM
					(String(now.getSeconds())).replace(/^\d$/,"0$&") ; //SS
			
			var logFileStdout = require("path").join(logDir,mfile_i.fileName+"."+DateNow+".stdout");
			var logFileStderr = require("path").join(logDir,mfile_i.fileName+"."+DateNow+".stderr");
		}
		{// save logFileStdout logFileStdErr
			function saveLog(theFile, data, description) {
				/*	
					theFile - full-filename
					data - buffer of data to write
					description - a meaningfull name for the data beiong writte, to the console statsu printouts
					
					This function will save data into theFile, printingout a small info containing description and the sucess/error of the write.
					
					Returns:
						1 if saved data ok
						0 if there was an error
				*/
				var savedOk=0;
				posix.open(	theFile,  /*process.O_WRONLY | process.O_APPEND | process.O_CREAT */  process.O_WRONLY | process.O_CREAT, process.S_IRUSR | process.S_IWUSR ).
					addCallback(function (fd) {
							//On success: fd = filedescriptor 
							posix.write(fd, data).
								addCallback( function(nbytes) {
									sys.puts(">>>> "+description+" saved in: "+theFile);
									savedOk=1;
									posix.close(fd);
								}).
								addErrback( function () {
									sys.puts(">> ERROR: could not write "+description+" into file: "+theFile);
									sys.puts("--- Starting to print "+description+":\n"+data+"=== End of "+description);
									savedOk=0;
									posix.close(fd);
								});
					}).
					addErrback(function () {
							//On error (no arguments) 
							sys.puts(">> ERROR: could not open file \""+theFile + "\"");
							sys.puts("---Starting to print "+description+":\n"+data+"===End of "+description);
							savedOk=0;
					});
			}			
			if (stdout.length > 0 )
				saveLog(logFileStdout, stdout, "Stdout");
			if (stderr.length > 0 ) 
				saveLog(logFileStderr, stderr, "Stderr");
		}
		
	}
	if (mfiler.addIfNewFile(mfile_i)) {
		//mfile_i is a new file!
		sys.puts(">>("+ (new Date()) + ") Detected new file: "+mfile_i.fullFileName);
		{// execute in shell theComand with mfile_i.fullFileName as argument (ie, execute in shell theCommand + "  " +  mfile_i.fullFileName
			sys.exec(theCommand + " " + mfile_i.fullFileName).
			addCallback( // fired if "sucess" event
				 function (stdout, stderr) {
					saveLogsStdoutStderr(stdout, stderr, logDir, mfile_i)
				}
			).
			addErrback( // fired if "error" event
				function (err, stdout, stderr) {
					saveLogsStdoutStderr(stdout, stderr, logDir, mfile_i)
				}
			);
		}
	}
}



// "Main loop"
var mfiler=new mFiler();
{// At startup, parse directory userDefined.target.dirPath to enqueue already existing files
	scanDirForExtFiles(userDefined.target.dirPath, userDefined.target.fileExt, 
		function(mfile_i){
			mfiler.addIfNewFile(mfile_i);
			return;
		}
	);
}
{// hook function handler, to be called when a new file is created in directory userDefined.target.dirPath, and execute userDefined.command on each of the new files with extension userDefined.target.fileExt
  process.watchFile(userDefined.target.dirPath, 
    function(){
      // Called when new file is created in directory userDefined.target.dirPath. Immune to delete/rename/modification of files, only runs for newly created files
      scanDirForExtFiles(userDefined.target.dirPath, userDefined.target.fileExt, executeCommandIfNewFile, userDefined.command, userDefined.logDir);
    }
  );
}
