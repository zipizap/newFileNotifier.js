This program was made to use inside a ftp-server, to automatically execute some command when a user uploads a new file. 


We will use it in work, so that a user can upload a file, wait some seconds,and then download a result.txt (which will be created by a program who processes the file uploaded)


So as node.js is asynchronous and event-based, and a new hype, it was a simple task to program withit and gain some feeling of what node.js is about.


The program made will monitor a directory, for the creation of new files with a known extension.  
When such a new file is detected, a command will be executed on that file.   
The stdout/stderr of the command execution will be piped to stdout/stderr.  

Both the directory to monitor, the extension of the file to be detected, and the command to be executed are supplied as arguments of the program.




# INSTALL 

  - you need to have node.js installed version >= v0.1.27-1-ge5a41a7  
    In debian/ubuntu you can do this very easily as explained in http://www.nakedjavascript.com/going-evented-with-nodejs which for convenience I will try to resume here:  

        sudo apt-get install build-essential git-core  
        git clone git://github.com/ry/node.git  
        cd node  
        ./configure  
        make  
        sudo make install  

  - you need to get a copy of this programs source from github (it's only one file, the rest are temporary notes)     
        `git clone git://github.com/zipizap/newFileNotifier.js.git`

  - and now you can simply run the script:  

        cd newFileNotifier.js
        node newFileNotifier.js




#  USAGE  

## EXAMPLE 0: Usage as can be seen when running in a console without arguments
```
$ node newFileNotifier.js
   newFileNotifier.js v1.0 [https://github.com/zipizap/newFileNotifier.js] 

   Usage:
      node newFileNotifier.js <directory> <file-extension> <command> <logsDir>

   This program, will continuously monitor the <directory> for any new files created with extension <file-extension>
   For each new file detected, it will create one child process executing:
      "<command> NEW_FILE", where NEW_FILE will be substituted by the absolute path of the new file detected
   After executing the command, the stoud and stderr of the execution will be logged inside the directory <logsDir>
   This program, existing through node.js, has native asynchronous calls, meaning the monitoring and child-processes execution is non-blocking, memory-efficient and parallel
   Note that this program will only monitor for new files creation, which does not include if an existing file is renamed to a new name (it continues to be an existing file, only with a different file name)

   Arguments:
     <directory>
          The directory to be monitored for new files. It can be a full or relative path to the directory
     <file-extension>
          All the new files created within <directory> will be detected, but only the new files with the same extension as <file-extension> will be processed (all others will be ignored).
          If the extension is defined as ".*" then any extension will be detected.
     <command>
          The command to be run in the child-process. This program will be called with one argument - the new file's full path
     <logsDir>
          The directory were it will be saved the logs of the command execution. The logs will be one file for stdout and another file for stderr (if it exists)
   Example:
     node newFileNotifier.js ./testDir zip "ls -l" "./testLogs"
      (now create a new file "touch $(date +%Y%m%d-%H%M%S).zip" inside testDir and the see the output of the program)

   TODO: for each command executed, both stdout and stderr will be piped. It may be a good idea to create log files for recording stdoud and stdin of each execution instead of just printing them

   ERROR: Too few arguments!!!
```

#EXAMPLE 1: 
  Monitor for new files created (uploaded) inside "/FtpPublic", with extension ".zip", and unzip them into "/FtpPublic/Unziped". The logs will be saved inside "/logs"

```
$ node newFileNotifier.js "/FtpPublic" zip "unzip -o -d /FtpPublic/Unziped" "/logs"
```





# Comments, notes and impressions #

## IMPRESSION 0: the code runs blazing fast, with a very small memory footprint on the server.   
The event-based nature is ++very well++ suited for long-running server-side daemons. 

## IMPRESSION 1: there seems to be a limit of around  350-450 simultaneous files  
 - number of parallel new files detected without crashing the program seems to be around 350-450 files  
   You can try this for yourself

      In console 1 run newFileNotifier.js in this fashion:
        `node newFileNotifier.js testDir/ ".*" "rm -f" testLogs`

      Now, in console 2 clean any existing files and create 1000 new empty files in testDir:
        `cd testDir && rm -rf * && touch {1..5000} `

   Now look at newFileNotifier.js output: it probably crashed indicating "pipe(): Too many open files"

   This limitation comes from the node.js core libraries, and although it seems a limitation, we need to first look and see what was the maximum number of files that newFileNotifier.js took care of simultaneously before crashing, to understand if that number is in reallity limitating the use that you want to give to newFileNotifier.js.

   So to see the number of simultaneous new files detected before crashing goto console 2 and count the number of files that were not deleted:

      Again back to console 2 (which is inside testDir), we will count the existing files:
```
EXISTINGFILES=$(ls -1 | wc -l)
FILESPROCESSED=$[ 5000 - $EXISTINGFILES ]
echo $FILESPROCESSED
```

   In my computer, there were 434 FILESPROCESSED, so to the use that we pretend to give, it will be more than sufficient to have the program process 433 new files at a time - it would be great if the FTP server could upload 433 files at a time, but as can be seen this value is more than realistically enough, and so what seems to be a limitation, in my case, is in fact only a theorical limit, as in practice it is a more the sufficient value.
   
 - UPDATE 09/03/2010 
   As clarified by the comment made by chrisdew (see [1]) the file-limit refered in IMPRESSION1 is rooted with the OS file-descriptor-limit per process, and so does not have to do with NODE.JS

   Also, the javascript code I've written uses some wait() calls, which forces synchronous execution, and limits the number of simultaneous files 
   [1] http://www.nakedjavascript.com/going-evented-with-nodejs


##IMPRESSION 2:

  After having the program working OK with new files, I've seen that the queue logic implemented, which is used to discern  existing files from new files in the directory, is not the best - imagine the case when a user uploads a file and by some error in the file, it needs to re-uplaod the file again. In the second upload, the file will not be recognized as a new file, because it's filename is already registered in the queue, and so it would be skipped as an already existing file.

  To circumvent this problem, I've tried to add a stat.ctime attribute into mFile objects, in order to keep track of the date of change of each file, and do the compares including the ctime of each file, as a way to discern between files with same name but with different ctime values (basically, for re-uploaded files)

  Well, it did work and detected when a file with a same name as a previously existing file was again created, and also detected when a already existing file was modified/changed (which was not so much of a problem, more like an extended funcionallity)
  
  But, using with the FTP server didn't prove usefull, because ftp transfers are made in chunks of data, and so when each new chunk of data was written on the file, it's .ctime was updated, and the command was run on the still-incomplete-file-being-uploaded
  
  So, I've disabled the .ctime component from the program, and the program still does exactly the same as when I begun, and won't detect re-uploads. 
  
  For the moment this is ok for what we use it, and it was nice playing around with node, but cannot dedicate more time to this :)
  

  NOTE: while coding the ctime variant, I've used wait() functions which are synchronous, and as a result the max limit of simultaneous files has droped to 2, certainly because of poor-code-quality of my own.
  
  PD: If you are interested in programming events/notifications of file/directory-changes/creation/deletion, you may also like to check the "inotify" library [2], which comes along with standard linux kernel distributions - it's very powerfull, it's in C but there are bindings to other programming languages as Ruby, PHP, Java, etc

  [2] http://en.wikipedia.org/wiki/Inotify

